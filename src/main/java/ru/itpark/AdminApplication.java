package ru.itpark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itpark.entity.AccountEntity;
import ru.itpark.entity.ProductEntity;
import ru.itpark.repository.AccountRepository;
import ru.itpark.repository.ProductRepository;

import java.util.List;

@SpringBootApplication
public class AdminApplication {

    public static void main(String[] args) {

        var context = SpringApplication.run(AdminApplication.class, args);

        context
                .getBean(ProductRepository.class)
                .saveAll(List.of(
                        new ProductEntity(
                                1,
                                "Чизбургер",
                                "Рубленый бифштекс из натуральной цельной говядины с кусочками сыра «Чеддер» на карамелизованной булочке, заправленной горчицей, кетчупом, луком и кусочком маринованного огурчика.",
                                50,
                                "cheeseburger.png"),
                        new ProductEntity(
                                2,
                                "Гамбургер",
                                "Рубленый бифштекс из натуральной цельной говядины на карамелизованной булочке, заправленной горчицей, кетчупом, луком и кусочком маринованного огурчика.",
                                48,
                                "hamburger.png"),
                        new ProductEntity(
                                3,
                                "Картофель фри",
                                "Вкусные, обжаренные в растительном фритюре и слегка посоленные палочки картофеля.",
                                50,
                                "fries.png"),
                        new ProductEntity(
                                4,
                                "Картофель по-деревенски",
                                "Вкусные, обжаренные в растительном фритюре ломтики картофеля со специями.",
                                72,
                                "potato.png"),
                        new ProductEntity(
                                5,
                                "Цезарь Ролл",
                                "Обжаренная куриная котлета, панированная в сухарях, свежий салат, ломтик помидора и зрелый сыр в пресной пшеничной лепешке, заправленной специальным соусом.",
                                164,
                                "cesar_roll.png"),
                        new ProductEntity(
                                6,
                                "Чикен Макнагетс",
                                "Обжаренные в растительном фритюре кусочки куриного мяса, панированные в сухарях, которые подаются с горчичным, кисло-сладким, барбекю соусом или соусом Карри. С каждой порцией два соуса по специальной сниженной цене.",
                                150,
                                "chicken_mcnaggets.png")
                        ));



        var encoder=context.getBean(PasswordEncoder.class);
        context
                .getBean(AccountRepository.class)
                .saveAll(List.of(
                        new AccountEntity(
                                0,
                                "vasya",
                                encoder.encode("vasya"),
                                List.of(new SimpleGrantedAuthority("ADMIN")),
                                true,
                                true,
                                true,
                                true
                        ),
                        new AccountEntity(
                                0,
                                "petya",
                                encoder.encode("petya"),
                                List.of(new SimpleGrantedAuthority("ADMIN")),
                                true,
                                true,
                                true,
                                true
                        )

                ));


    }
}
