package ru.itpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itpark.dto.ProductEditDTO;
import ru.itpark.dto.ProductSaveDTO;
import ru.itpark.service.ProductService;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private final ProductService productService;

    public AdminController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("items", productService.findAll());
        return "admin/adminpanel";
    }

    @PostMapping("/{id}/delete")
    public String deleteById(@PathVariable int id){
        productService.deleteById(id);
        return "redirect:/admin";
    }

    @PostMapping
    public String save(@Valid @ModelAttribute ProductSaveDTO dto){
        productService.save(dto);
        return "redirect:/admin";
    }

    @RequestMapping("/search")
    public String findByName(
            @RequestParam String name,
            Model model
    ) {
        model.addAttribute("search",name);
        model.addAttribute("items", productService.findByName(name));
        return "admin/adminpanel";
    }

    @PostMapping("/{id}/edit")
    public String edit(@PathVariable int id, @Valid @ModelAttribute ProductEditDTO dto){
        productService.editById(id, dto);
        return "redirect:/admin";
    }

    @GetMapping("/{id}/edit")
    public String getEdit(@PathVariable int id, Model model){
        model.addAttribute("item",productService.findById(id));
        return "admin/edit";
    }




}
