package ru.itpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itpark.dto.ProductSaveDTO;
import ru.itpark.service.ProductService;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class SiteController {
    private final ProductService productService;

    public SiteController(ProductService productService) {

        this.productService = productService;
    }

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("items",productService.findAll());
        return "products";
    }

    @RequestMapping("/search")
    public String findByName(
            @RequestParam String name,
            Model model
    ) {
        model.addAttribute("search",name);
        model.addAttribute("items", productService.findByName(name));
        return "products";
    }

    @GetMapping("/{id:\\d+}")
    public String get(@PathVariable int id, Model model){
        model.addAttribute("item",productService.findById(id));
        return "product";
    }



}
