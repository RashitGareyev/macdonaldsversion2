package ru.itpark.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductEditDTO {
    @NotNull
    @Size(min=3, max=50)
    private String name;
    @NotNull
    private String description;
    @NotNull
    private int price;
    @NotNull//эта аннотация не даст сделать нормально edit
    private MultipartFile img;
}
