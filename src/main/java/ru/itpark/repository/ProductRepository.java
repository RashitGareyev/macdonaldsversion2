package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itpark.dto.ProductEditDTO;
import ru.itpark.entity.ProductEntity;

import java.util.List;

public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {



    @Query("SELECT e FROM ProductEntity e WHERE LOWER(e.name) LIKE LOWER(:name)")
    List<ProductEntity> findByName(@Param("name") String name);

}
