package ru.itpark.service;

import ru.itpark.dto.ProductEditDTO;
import ru.itpark.dto.ProductSaveDTO;
import ru.itpark.entity.ProductEntity;

import java.util.List;

public interface ProductService {
    List<ProductEntity> findAll();

    void deleteById(int id);

    void save(ProductSaveDTO dto);


    List<ProductEntity> findByName(String name);

    void editById(int id, ProductEditDTO dto);

    ProductEntity findById(int id);

}
