package ru.itpark.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import ru.itpark.dto.ProductEditDTO;
import ru.itpark.dto.ProductSaveDTO;
import ru.itpark.entity.ProductEntity;
import ru.itpark.exception.MediaRemoveException;
import ru.itpark.exception.MediaUploadException;
import ru.itpark.exception.UnsupportedMediaTypeException;
import ru.itpark.exception.ProductNotFoundException;
import ru.itpark.repository.ProductRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final Path mediaPath;


    public ProductServiceImpl(
            ProductRepository productRepository,
            @Value("${app.media-path}") String mediaPath
    ) {
        this.productRepository = productRepository;
        this.mediaPath = Paths.get(mediaPath);
    }

    @Override
    public List<ProductEntity> findAll() {

        return productRepository.findAll();
    }

    @Override
    public void deleteById(int id) {
        var entity = productRepository.findById(id).orElseThrow(ProductNotFoundException::new);
        Path target = mediaPath.resolve(entity.getImg());

        try {
            Files.deleteIfExists(target);
        } catch (IOException e) {
            throw new MediaRemoveException(e);
        }

        productRepository.delete(entity);
    }

    @Override
    public void save(ProductSaveDTO dto) {
        var contentTypeImg = dto.getImg().getContentType();

        String name = UUID.randomUUID().toString();
        String ext;

        if (MediaType.IMAGE_JPEG_VALUE.equals(contentTypeImg)) {
            ext = ".jpg";
        } else if (MediaType.IMAGE_PNG_VALUE.equals(contentTypeImg)) {
            ext = ".png";
        } else {
            throw new UnsupportedMediaTypeException(contentTypeImg);
        }

        Path target = mediaPath.resolve(name + ext);

        try {
            dto.getImg().transferTo(target.toFile());
        } catch (IOException e) {
            throw new MediaUploadException(e);
        }

        ProductEntity entity = new ProductEntity(productRepository.findAll().size()+1, dto.getName(), dto.getDescription(), dto.getPrice(), name + ext);
        productRepository.save(entity);
    }

    @Override
    public List<ProductEntity> findByName(String name) {

        return productRepository.findByName("%" + name + "%");
    }




    @Override
    public void editById(int id, ProductEditDTO dto) {
        ProductEntity initialEntity = productRepository.findById(id).orElseThrow(ProductNotFoundException::new);

        var contentTypeImg = dto.getImg().getContentType();

        String name;
        String ext;
        Path target;

        if (MediaType.IMAGE_JPEG_VALUE.equals(contentTypeImg)) {
            name = UUID.randomUUID().toString();
            ext = ".jpg";
            target = mediaPath.resolve(name + ext);
            try {
                dto.getImg().transferTo(target.toFile());
            } catch (IOException e) {
                throw new MediaUploadException(e);
            }
            ProductEntity entity = new ProductEntity(id, dto.getName(), dto.getDescription(), dto.getPrice(), name + ext);
            productRepository.save(entity);
        } else if (MediaType.IMAGE_PNG_VALUE.equals(contentTypeImg)) {
            name = UUID.randomUUID().toString();
            ext = ".png";
            target = mediaPath.resolve(name + ext);
            try {
                dto.getImg().transferTo(target.toFile());
            } catch (IOException e) {
                throw new MediaUploadException(e);
            }
            ProductEntity entity = new ProductEntity(id, dto.getName(), dto.getDescription(), dto.getPrice(), name + ext);
            productRepository.save(entity);
        } else if (dto.getImg().isEmpty()) {
            ProductEntity entity = new ProductEntity(id, dto.getName(), dto.getDescription(), dto.getPrice(), initialEntity.getImg());
            productRepository.save(entity);
        } else {
            throw new UnsupportedMediaTypeException(contentTypeImg);
        }

    }


    @Override
    public ProductEntity findById(int id) {
        return productRepository.findById(id).orElseThrow(ProductNotFoundException::new);
    }
}
